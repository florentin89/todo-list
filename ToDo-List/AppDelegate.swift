//
//  AppDelegate.swift
//  ToDo-List
//
//  Created by Florentin on 20/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import UIKit
import RealmSwift
import SCLAlertView

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        print(Realm.Configuration.defaultConfiguration.fileURL)
        do{
            _ = try Realm()
        } catch{
            SCLAlertView().showError("Error", subTitle: "Error inistailising new realm, \(error)")
        }
        return true
    }
}
