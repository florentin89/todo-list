//
//  CategoryViewController.swift
//  ToDo-List
//
//  Created by Florentin on 21/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework
import SCLAlertView

class CategoryViewController: SwipeTableViewController {
    
    // MARK: - ***** Properties *****
    let realm = try! Realm()
    var categories: Results<Category>?
    
    // MARK: - ***** Life Cycle Methods *****
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCategories()
    }
    
    //MARK: - ***** TableView Datasource Methods *****
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        if let category = categories?[indexPath.row]{
            
            cell.textLabel?.text = category.name
            
            guard let categoryColor = UIColor(hexString: category.color) else { fatalError() }
            
            cell.backgroundColor = UIColor(hexString: category.color)
            
            cell.textLabel?.textColor = ContrastColorOf(categoryColor, returnFlat: true)
            
        }
        
        return cell
    }
    
    //MARK: - ***** TableView Delegate Methods *****
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "goToItems", sender: self)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationVC = segue.destination as! ToDoListViewController
        
        if let indexPath = tableView.indexPathForSelectedRow {
            destinationVC.selectedCategory = categories?[indexPath.row]
        }
    }
    
    //MARK: - ***** Data Manipulation Methods *****
    
    //MARK: - Add new Categories
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add new category", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Category", style: .default) { (action) in
            
            Spinner.show("Loading Categories")
            
            let newCategory = Category()
            if !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
                newCategory.name = textField.text!
            }else{
                SCLAlertView().showError("Error", subTitle: "Category need to have a name !")
                Spinner.hide()
                return
            }
            newCategory.color = UIColor.randomFlat.hexValue()
            
            self.save(category: newCategory)
        }
        
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Create new category"
            textField = alertTextField
        }
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Save Categories
    func save(category: Category){
        
        do {
            try realm.write {
                realm.add(category)
            }
            Spinner.hide()
        }
        catch{
            SCLAlertView().showError("Error", subTitle: "Error saving context \(error)")
        }
        tableView.reloadData()
    }
    
    // MARK: - Load Categories
    func loadCategories(){
        
        categories = realm.objects(Category.self).sorted(byKeyPath: "name", ascending: true)
        
        tableView.reloadData()
    }
    
    // MARK: - Delete Data From Swipe
    override func updateModel(at indexPath: IndexPath) {
        
        if let categoryToDelete = categories?[indexPath.row] {
            do{
                try self.realm.write {
                    realm.delete(categoryToDelete)
                }
            } catch{
                SCLAlertView().showError("Error", subTitle: "Error deleting category, \(error)")
            }
        }
    }
}
