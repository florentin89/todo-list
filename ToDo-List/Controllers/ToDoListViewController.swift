//
//  ViewController.swift
//  ToDo-List
//
//  Created by Florentin on 20/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework
import SCLAlertView

class ToDoListViewController: SwipeTableViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: - ***** Properties *****
    var items: Results<Item>?
    let realm = try! Realm()
    var selectedCategory: Category? {
        didSet{
            loadItems()
        }
    }
    
    //MARK: - ***** Life Cycle Methods*****
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        title = selectedCategory?.name
        
        guard let colorHex = selectedCategory?.color else { fatalError("Selected category not exist.") }
        
        self.updateNavBar(withHexCode: colorHex)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.updateNavBar(withHexCode: "0096FF")
    }
    
    //MARK: - NavBar Setup Methods
    func updateNavBar(withHexCode colourHexCode: String){
        
        guard let navBar = navigationController?.navigationBar else {fatalError("Navigation Controller not exist.")}
        
        guard let navBarColor = UIColor(hexString: colourHexCode) else {fatalError("Navigation Bar not exist.") }
        
        navBar.barTintColor = navBarColor
        
        navBar.tintColor = ContrastColorOf(navBarColor, returnFlat: true)
        
        navBar.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: ContrastColorOf(navBarColor, returnFlat: true)]
        
        searchBar.barTintColor = navBarColor
    }
    
    //MARK: - ***** TableView DataSource Methods *****
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        if let item = items?[indexPath.row] {
            
            cell.textLabel?.text = item.title
            
            if let color = UIColor(hexString: selectedCategory!.color)?.darken(byPercentage: CGFloat(indexPath.row) / CGFloat(items!.count)){
                
                cell.backgroundColor = color
                cell.textLabel?.textColor = ContrastColorOf(color, returnFlat: true)
            }
            
            // Ternary operator example: value = condition ? valueIfTrue : valueIfFalse
            cell.accessoryType = item.done ? .checkmark : .none
        } else {
            cell.textLabel?.text = "No items added"
        }
        
        return cell
    }
    
    //MARK: - ***** TableView Delegate Methods *****
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let item = items?[indexPath.row] {
            do{
                try realm.write {
                    item.done = !item.done
                }
            } catch{
                SCLAlertView().showError("Error", subTitle: "Error saving done status, \(error)")
            }
        }
        
        tableView.reloadData()
        
        tableView.deselectRow(at: indexPath, animated: true) // after you selected a row will be deselected with animation
    }
    
    //MARK: - ***** Data Manipulation Methods *****
    //MARK: - Add New Items
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add new ToDo item", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Item", style: .default) { (action) in
            
            Spinner.show("Loading Items")
            
            if let currentCategory = self.selectedCategory{
                do{
                    try self.realm.write {
                        let newItem = Item()
                        if !(textField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
                            newItem.title = textField.text!
                        }
                        else{
                            SCLAlertView().showError("Error", subTitle: "Item need to have a name !")
                            Spinner.hide()
                            return
                        }
                        newItem.dateCreated = Date()
                        currentCategory.items.append(newItem)
                        Spinner.hide()
                    }
                } catch{
                    SCLAlertView().showError("Error", subTitle: "Error saving context \(error)")
                }
            }
            self.tableView.reloadData()
        }
        
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Create new item"
            textField = alertTextField
        }
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Load Items
    func loadItems(){
        
        items = selectedCategory?.items.sorted(byKeyPath: "title", ascending: true)
        tableView.reloadData()
        Spinner.hide()
    }
    
    // MARK: - Delete Data From Swipe
    override func updateModel(at indexPath: IndexPath) {
        
        if let itemToDelete = self.items?[indexPath.row] {
            do{
                try self.realm.write {
                    self.realm.delete(itemToDelete)
                }
            } catch{
                SCLAlertView().showError("Error", subTitle: "Error deleting item, \(error)")
            }
        }
    }
}

//MARK: - ***** SearchBar Delegate Methods *****
extension ToDoListViewController: UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        Spinner.show("Loading items")
        items = items?.filter("title CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "dateCreated", ascending: true)
        tableView.reloadData()
        Spinner.hide()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            Spinner.show("Loading items")
            loadItems()
            DispatchQueue.main.async {
                searchBar.resignFirstResponder() // Dismiss cursor from search bar
            }
        }
    }
}
